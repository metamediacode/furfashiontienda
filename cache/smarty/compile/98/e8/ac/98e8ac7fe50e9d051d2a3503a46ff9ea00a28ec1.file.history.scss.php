<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\history.scss" */ ?>
<?php /*%%SmartyHeaderCode:1667553a035b512ad71-51663514%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '98e8ac7fe50e9d051d2a3503a46ff9ea00a28ec1' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\history.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1667553a035b512ad71-51663514',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b5135c92_97343090',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b5135c92_97343090')) {function content_53a035b5135c92_97343090($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
/************************************************************
				History Page Styles
************************************************************/
#order-detail-content table {
	.return_quantity_buttons {
		margin-top: 3px;
		a {
			display: none;
			float: left;
			&+a {
				margin-left: 3px;
			}	
		}
	}
	.order_qte_input { 
		display: none;
		width: 57px;
		height: 27px;
		line-height: 27px;
		padding: 0;
		text-align: center;
	}
	label {
		font-weight: 400;
	}
}

.table {
	td.history_detail {
		a + a {
			margin-left: 14px;	
		}
	}
	td.step-by-step-date {
		width: 105px;	
	}
	tfoot {
		
		strong {
			color: $base-text-color;	
		}
	}
}

.info-order {
	i {
		font-size: 20px;
		&.icon-gift {
			color: #f13340;
		}
		&.icon-repeat {
			color: #55c65e;
		}
	}
}

#sendOrderMessage {
	margin-bottom: 30px;
	select.form-control {
		width: 263px;
	}
}<?php }} ?>