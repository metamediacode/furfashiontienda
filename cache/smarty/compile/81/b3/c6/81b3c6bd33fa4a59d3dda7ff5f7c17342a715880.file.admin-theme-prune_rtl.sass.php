<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\schemes_rtl\admin-theme-prune_rtl.sass" */ ?>
<?php /*%%SmartyHeaderCode:2910753a035bcbcd6d1-30633117%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81b3c6bd33fa4a59d3dda7ff5f7c17342a715880' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\schemes_rtl\\admin-theme-prune_rtl.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2910753a035bcbcd6d1-30633117',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bcbcff20_38668426',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bcbcff20_38668426')) {function content_53a035bcbcff20_38668426($_smarty_tpl) {?>// Prune: PrestaShop Admin Theme RTL
//    _         _            _      _          _      
//  _/\\___   _/\\___   ___ /\\   _/\\___   __/\\___  
// (_   _ _))(_   _  ))/  //\ \\ (_      ))(_  ____)) 
//  /  |))\\  /  |))// \:.\\_\ \\ /  :   \\ /  ._))   
// /:. ___// /:.    \\  \  :.  ///:. |   ///:. ||___  
// \_ \\     \___|  // (_   ___))\___|  // \  _____)) 
//   \//          \//    \//          \//   \//  
//
// by Vincent Terenti

@import "modules/colors"
$fa-font-path: "../../fonts"

// Colors
$main-color: $grayCharcoal
$primary-color: $blueZurb
$secondary-color: #824868
$bg-content-color: $grayTaupe

// Typography
$url-font-content: "//fonts.googleapis.com/css?family=Droid+Sans:300,400,700"
$url-font-content-name : 'Droid Sans'
$url-font-headings: "//fonts.googleapis.com/css?family=Ubuntu+Condensed" !default
$url-font-headings-name: 'Ubuntu Condensed' !default

// Let's go party!
@import "admin-theme_rtl"<?php }} ?>