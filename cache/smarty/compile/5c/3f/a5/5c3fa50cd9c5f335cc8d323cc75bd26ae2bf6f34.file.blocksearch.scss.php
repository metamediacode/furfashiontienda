<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\modules\blocksearch\blocksearch.scss" */ ?>
<?php /*%%SmartyHeaderCode:22853a035b522e868-88444431%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5c3fa50cd9c5f335cc8d323cc75bd26ae2bf6f34' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\modules\\blocksearch\\blocksearch.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22853a035b522e868-88444431',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b5235539_48007986',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b5235539_48007986')) {function content_53a035b5235539_48007986($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
#search_block_top {
	padding-top: 50px;
	#searchbox {
		float: left;
		width: 100%;
	}
	.btn.button-search {
		background: $dark-background;
		display: block;
		position: absolute;
		top: 0;
		right: 0;
		border: none;
		color: $light-text-color;
		width: 50px;
		text-align: center;
		padding: 10px 0 11px 0;
		span {
			display: none;
		}
		&:before {
			content: "\f002";
			display: block;
			font-family: $font-icon;
			font-size: 17px;
			width: 100%;
			text-align: center;
		}
		&:hover {
			color: #6f6f6f
		}
	}
	#search_query_top {
		display: inline;
		padding: 0 13px;
		height: 45px;
		line-height: 45px;
		background: $base-box-bg;
		margin-right: 1px;
	}
}

.ac_results {
	background: $light-background;
	border: 1px solid $base-border-color;
	width: 271px;
	margin-top: -1px;
	li {
		padding: 0 10px;
		font-weight: normal;
		color: #686666;
		font-size: 13px;
		line-height: 22px;
		&.ac_odd {
			background: $light-background;
		}
		&:hover, &.ac_over {
			background: $base-box-bg;	
		}
	}
}	

form#searchbox {
	position: relative;
	label {
		color: $base-text-color;
	}
	input#search_query_block {
		margin-right:10px;
		max-width:222px;
		margin-bottom:10px;
		display:inline-block;
		float:left;
	}
	.button.button-small {
		float:left;
		i {
			margin-right:0;
		}
	}
}<?php }} ?>