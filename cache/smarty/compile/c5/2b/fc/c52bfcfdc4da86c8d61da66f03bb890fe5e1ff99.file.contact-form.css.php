<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:51
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\css\contact-form.css" */ ?>
<?php /*%%SmartyHeaderCode:2279553a035afd1e367-40681935%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c52bfcfdc4da86c8d61da66f03bb890fe5e1ff99' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\css\\contact-form.css',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2279553a035afd1e367-40681935',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035afd26379_79480783',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035afd26379_79480783')) {function content_53a035afd26379_79480783($_smarty_tpl) {?>/**************************************************************************
					Contact Page Styles
**************************************************************************/
.contact-title {
  margin: -5px 0 27px 0;
  line-height: 23px; }
  .contact-title i {
    font-size: 23px;
    color: #c0c0c0;
    padding: 0 8px 0 0; }

.contact-form-box {
  padding: 23px 0 0 0;
  margin: 0 0 30px 0;
  background: url(../img/contact-form.png) repeat-x white;
  -webkit-box-shadow: rgba(0, 0, 0, 0.17) 0px 5px 13px;
  -moz-box-shadow: rgba(0, 0, 0, 0.17) 0px 5px 13px;
  box-shadow: rgba(0, 0, 0, 0.17) 0px 5px 13px; }
  .contact-form-box fieldset {
    padding: 0 19px 21px 19px;
    background: url(../img/form-contact-shadow.png) center bottom no-repeat;
    background-size: contain; }
  .contact-form-box label {
    margin: 0 0 6px 0; }
  .contact-form-box .page-subheading {
    padding-left: 0px;
    border: none;
    margin-bottom: 0; }
  .contact-form-box .col-md-3 {
    padding-left: 0; }
    @media (max-width: 991px) {
      .contact-form-box .col-md-3 {
        padding-right: 0; } }
  .contact-form-box .col-md-9 {
    padding-right: 0; }
    @media (max-width: 991px) {
      .contact-form-box .col-md-9 {
        padding-left: 0; } }
  .contact-form-box #desc_contact0 {
    display: none; }
  .contact-form-box .form-group {
    margin-bottom: 7px; }
  .contact-form-box textarea {
    height: 257px; }
  .contact-form-box .submit {
    margin-top: 13px; }
  .contact-form-box select.form-control {
    max-width: 270px;
    width: 100%; }
  .contact-form-box input.form-control {
    max-width: 270px; }
  @media (min-width: 992px) and (max-width: 1199px) {
    .contact-form-box div.uploader span.filename {
      width: 114px; } }<?php }} ?>