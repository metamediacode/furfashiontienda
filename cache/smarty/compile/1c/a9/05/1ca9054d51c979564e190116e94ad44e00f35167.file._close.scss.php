<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\vendor\bootstrap-sass\_close.scss" */ ?>
<?php /*%%SmartyHeaderCode:2017253a035bcd6e8f8-37806549%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ca9054d51c979564e190116e94ad44e00f35167' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\vendor\\bootstrap-sass\\_close.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2017253a035bcd6e8f8-37806549',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bcd72311_56354003',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bcd72311_56354003')) {function content_53a035bcd72311_56354003($_smarty_tpl) {?>//
// Close icons
// --------------------------------------------------


.close {
  float: right;
  font-size: ($font-size-base * 1.5);
  font-weight: $close-font-weight;
  line-height: 1;
  color: $close-color;
  text-shadow: $close-text-shadow;
  @include opacity(.2);

  &:hover,
  &:focus {
    color: $close-color;
    text-decoration: none;
    cursor: pointer;
    @include opacity(.5);
  }

  // [converter] extracted button& to button.close
}

// Additional properties for button version
// iOS requires the button element instead of an anchor tag.
// If you want the anchor version, it requires `href="#"`.
button.close {
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;
  -webkit-appearance: none;
}
<?php }} ?>