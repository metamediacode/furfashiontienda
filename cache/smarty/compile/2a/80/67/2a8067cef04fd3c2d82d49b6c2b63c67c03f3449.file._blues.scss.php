<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_blues.scss" */ ?>
<?php /*%%SmartyHeaderCode:1486653a035bc8d2333-41808659%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2a8067cef04fd3c2d82d49b6c2b63c67c03f3449' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_blues.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1486653a035bc8d2333-41808659',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc8d4786_89895435',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc8d4786_89895435')) {function content_53a035bc8d4786_89895435($_smarty_tpl) {?>// Blue 32
// -------------------------

//Defaults

$blue:             		#0066FF !default;
$blueDark:         		#003D99 !default;
$blueDarker:       		#002966 !default;
$blueLight:        		#4D94FF !default;
$blueLighter:      		#80B2FF !default;
$blueDull:         		#0066CC !default;
$blueDuller:       		#336699 !default;
$blueBright:       		#3399FF !default;
$blueBrighter:     		#70B8FF !default;


//Variations

$blueAirforce:        	#5d8aa8 !default;
$blueAzure:        		#007fff !default;
$blueBaby:        		#89cff0 !default;
$blueFrench:        	#318ce7 !default;
$blueCeleste:        	#b2ffff !default;
$blueColumbia:        	#75b2dd !default;
$blueSky:        		#73c2fb !default;
$blueMidnight:        	#191970 !default;
$blueNavy:        		#000080 !default;
$blueRoyal:        		#4169e1 !default;
$blueSapphire:        	#0f52ba !default;
$blueSteel:        		#4682b4 !default;

//Brands

$blueBehance:        	#053eff !default;
$blueDropbox:        	#3d9ae8 !default;
$blueFacebook:        	#3b5998 !default;
$blueFlickr:        	#0063dc !default;
$blueFoursquare:        #25a0ca !default;
$blueLinkedIn:        	#0e76a8 !default;
$blueRdio:        		#008fd5 !default;
$blueSkype:        		#00aff0 !default;
$blueTwitter:        	#00a0d1 !default;
$blueVimeo:        		#86c9ef !default;
$blueVirb:        		#06afd8 !default;

// * * * Premium * * *
// -------------------------


//Dulux 13

$blueSea:        		#668ebf !default;
$blueBabe:        		#96b5d4 !default;
$blueReflection:        #9ab7b3 !default;
$blueHoliday:        	#0097C8 !default;
$blueStonewash:        	#648e9c !default;
$blueCyan:      		#36a0ca !default;
$blueSapphire:    		#2c405b !default;
$blueLake:        		#476a88 !default;
$blueBreton:        	#4d5965 !default;
$blueSeduction:        	#4C84B8 !default;
$blueCloud:      		#6883B6 !default;


//Zurb Foundation 1

$blueZurb:        		#2ba6cb !default;



<?php }} ?>