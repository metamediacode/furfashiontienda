<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\modules\blockuserinfo\blockuserinfo.scss" */ ?>
<?php /*%%SmartyHeaderCode:2797753a035b5291442-67333361%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8162f6445b73ef332fcf013513dae67efa038c22' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\modules\\blockuserinfo\\blockuserinfo.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2797753a035b5291442-67333361',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b5293a03_06680154',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b5293a03_06680154')) {function content_53a035b5293a03_06680154($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
.header_user_info {
	float: right;
	border-left: 1px solid #515151;
	border-right: 1px solid #515151;
	a {
		color: $light-text-color;
		font-weight: bold;
		display: block;
		padding: 8px 9px 11px 8px;
		cursor: pointer;
		line-height:18px;
		@media (max-width: $screen-xs - 1) {
			font-size: 11px;
		}
		&:hover, &.active {
			background: #2b2b2b;
		}
	}
}<?php }} ?>