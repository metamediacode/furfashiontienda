<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\font-awesome\_mixins.scss" */ ?>
<?php /*%%SmartyHeaderCode:2957553a035b4f108e6-29785179%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4373536c4aa611c8c9d09e6152b929a99ebe7a1d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\font-awesome\\_mixins.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2957553a035b4f108e6-29785179',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'height' => 0,
    'base' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4f17af6_96987666',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4f17af6_96987666')) {function content_53a035b4f17af6_96987666($_smarty_tpl) {?>// Mixins
// --------------------------

@mixin icon($icon) {
  @include icon-FontAwesome();
  content: $icon;
}

@mixin icon-FontAwesome() {
  font-family: FontAwesome;
  font-weight: normal;
  font-style: normal;
  text-decoration: inherit;
  -webkit-font-smoothing: antialiased;
  *margin-right: .3em; // fixes ie7 issues
}

@mixin border-radius($radius) {
  -webkit-border-radius: $radius;
  -moz-border-radius: $radius;
  border-radius: $radius;
}

@mixin icon-stack($width: 2em, $height: 2em, $top-font-size: 1em, $base-font-size: 2em) {
  .icon-stack {
    position: relative;
    display: inline-block;
    width: $width;
    height: $height;
    line-height: $width;
    vertical-align: -35%;
    [class^="icon-"],
    [class*=" icon-"] {
      display: block;
      text-align: center;
      position: absolute;
      width: 100%;
      height: 100%;
      font-size: $top-font-size;
      line-height: inherit;
      *line-height: $height;
    }
    .icon-stack-base {
      font-size: $base-font-size;
      *line-height: #<?php echo $_smarty_tpl->tpl_vars['height']->value/$_smarty_tpl->tpl_vars['base']->value-'font'-'size';?>
em;
    }
  }
}
<?php }} ?>