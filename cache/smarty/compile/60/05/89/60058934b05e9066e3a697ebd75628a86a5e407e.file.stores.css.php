<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:52
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\css\stores.css" */ ?>
<?php /*%%SmartyHeaderCode:2471853a035b05e2e62-53974166%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '60058934b05e9066e3a697ebd75628a86a5e407e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\css\\stores.css',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2471853a035b05e2e62-53974166',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b05ec750_52433158',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b05ec750_52433158')) {function content_53a035b05ec750_52433158($_smarty_tpl) {?>/***********************************************************
					Stores Page Styles
************************************************************/
#stores #map {
  width: 100%;
  height: 447px;
  margin-bottom: 26px; }
#stores input#addressInput {
  display: inline-block;
  width: 269px; }
#stores .address-input {
  float: left;
  margin-right: 19px; }
  @media (max-width: 767px) {
    #stores .address-input {
      width: 100%;
      margin: 0 0 20px 0; } }
  #stores .address-input label {
    margin-right: 5px; }
#stores .radius-input {
  float: left;
  line-height: 23px;
  margin-right: 20px; }
  #stores .radius-input label,
  #stores .radius-input .selector {
    float: left; }
  #stores .radius-input label {
    margin-right: 10px; }
  #stores .radius-input #radiusSelect {
    width: 78px; }
#stores .store-content {
  padding-bottom: 30px; }
#stores .store-content-select .selector {
  visibility: hidden;
  display: none; }
#stores .store-content-select.active {
  padding-top: 30px;
  margin-bottom: 30px;
  border-top: 1px solid #d6d4d4; }
  #stores .store-content-select.active .selector {
    visibility: visible;
    display: block; }
#stores .store-title {
  margin-bottom: 16px; }
#stores label {
  font-weight: normal;
  color: #777777; }
#stores #stores_loader {
  display: none;
  margin-left: 5px; }
#stores #locationSelect {
  max-width: 356px;
  visibility: hidden; }
#stores #stores-table {
  display: none; }
  #stores #stores-table tbody td {
    padding: 20px; }
    #stores #stores-table tbody td.num {
      background: #fbfbfb;
      width: 46px; }
    #stores #stores-table tbody td.name span {
      padding-left: 15px; }
    @media (max-width: 767px) {
      #stores #stores-table tbody td.name img {
        display: none; } }

.store-image img {
  width: 100%;
  min-width: 100px; }
<?php }} ?>