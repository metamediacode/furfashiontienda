<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\partials\_footer.sass" */ ?>
<?php /*%%SmartyHeaderCode:2567553a035bca3f0e9-60490876%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4f7a9b9f00ed043e69d90a4d17347a1b9adb6b92' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\partials\\_footer.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2567553a035bca3f0e9-60490876',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bca41231_77162304',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bca41231_77162304')) {function content_53a035bca41231_77162304($_smarty_tpl) {?>#footer
	z-index: 600
	display: block
	width: 100%
	background-color: rgba(black,0.8)
	color: #aaa
	position: fixed
	bottom: 0
	height: 50px
	line-height: 50px
	@include animate(fadeInUp, 0.3s)
	a
		color: #ccc
	&.hide
		display: none!important
	#go-top
		position: fixed
		bottom: 10px
		height: 30px
		width: 30px
		text-align: center
		line-height: 30px
		z-index: 9003
		display: block
		padding: 0px 6px
		margin: -2px 0px 0px
		text-decoration: none
		color: #fff
		cursor: pointer
		font-size: 16px
		background-color: $main-color
		@include right(10px)
		&:hover
			background-color: $brand-primary
	.social-networks
		margin: 4px 0 0 0
	a.footer_link
		color: $brand-primary
		&:hover
			color: white
			text-decoration: none
		i
			font-size: $icon-size-base
			color: white
	.footer-contact
		@include padding(7px, 0, 0, 0)
		p
			line-height: 1.3em
			margin: 0 0 6px
			strong
				font-weight: 400
				color: white

a.link-social
	i
		font-size: 28px
		width: 34px
		height: 34px
		display: inline-block
		line-height: 34px!important
		text-align: center
		@include margin(0, 4px, 0, 0)
		@include border-radius(30px)
	&:hover
		text-decoration: none

.link-twitter i
	color: white
	background-color: #7CCEEF
	&:hover
		color: #7CCEEF
		background-color: white
.link-facebook i
	color: white
	background-color: #557DBB
	&:hover
		color: #557DBB
		background-color: white
.link-github i
	color: black
	background-color: white
	&:hover
		color: white
		background-color: black
.link-google i
	color: white
	background-color: #E6644E
	&:hover
		color: #E6644E
		background-color: white
<?php }} ?>