<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\schemes\admin-theme-prune.sass" */ ?>
<?php /*%%SmartyHeaderCode:501453a035bcb8b344-60459221%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd8f0d8a48c2c1423720c858f6777273ea90415c7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\schemes\\admin-theme-prune.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '501453a035bcb8b344-60459221',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bcb8cfc1_64272333',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bcb8cfc1_64272333')) {function content_53a035bcb8cfc1_64272333($_smarty_tpl) {?>// Prune: PrestaShop Admin Theme
//    _         _            _      _          _      
//  _/\\___   _/\\___   ___ /\\   _/\\___   __/\\___  
// (_   _ _))(_   _  ))/  //\ \\ (_      ))(_  ____)) 
//  /  |))\\  /  |))// \:.\\_\ \\ /  :   \\ /  ._))   
// /:. ___// /:.    \\  \  :.  ///:. |   ///:. ||___  
// \_ \\     \___|  // (_   ___))\___|  // \  _____)) 
//   \//          \//    \//          \//   \//  
//
// by Vincent Terenti

@import "modules/colors"
$fa-font-path: "../../fonts"

// Colors
$main-color: $grayCharcoal
$primary-color: $blueZurb
$secondary-color: #824868
$bg-content-color: $grayTaupe

// Typography
$url-font-content: "//fonts.googleapis.com/css?family=Droid+Sans:300,400,700"
$url-font-content-name : 'Droid Sans'
$url-font-headings: "//fonts.googleapis.com/css?family=Ubuntu+Condensed" !default
$url-font-headings-name: 'Ubuntu Condensed' !default

// Let's go party!
@import "admin-theme"<?php }} ?>