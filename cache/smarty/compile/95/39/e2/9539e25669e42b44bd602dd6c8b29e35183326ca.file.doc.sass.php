<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\doc.sass" */ ?>
<?php /*%%SmartyHeaderCode:1952753a035bc8a7392-30362060%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9539e25669e42b44bd602dd6c8b29e35183326ca' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\doc.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1952753a035bc8a7392-30362060',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc8a9f60_05564626',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc8a9f60_05564626')) {function content_53a035bc8a9f60_05564626($_smarty_tpl) {?>// Make sure the charset is set appropriately
@charset "UTF-8"
//Compass is required to pre-compile this stylesheets to .css

@import "modules/colors"
@import "modules/web-fonts"

$body-bg: white
$text-color: #7a7a7a
$brand-primary: #585984
$font-size-base: 13px
//** Global textual link color.
$link-color: $brand-primary
//** Link hover color set via `darken()` function.
$link-hover-color: darken($link-color, 15%)
$font-family-sans-serif:  "Open Sans", Helvetica, Arial, sans-serif
$font-family-base: $font-family-sans-serif
$headings-color: #DB5475

//Bootstrap Core : Variables + Mixins + Reset + Print
@import "vendor/bootstrap-sass/variables"
@import "vendor/bootstrap-sass/mixins"
@import "vendor/bootstrap-sass/normalize"
@import "vendor/bootstrap-sass/print"

//Core CSS
@import "vendor/bootstrap-sass/scaffolding"
@import "vendor/bootstrap-sass/type"
@import "vendor/bootstrap-sass/code"
//@import "vendor/bootstrap-sass/grid"
//@import "vendor/bootstrap-sass/tables"
//@import "vendor/bootstrap-sass/forms"
//@import "vendor/bootstrap-sass/buttons"
//Components
//@import "vendor/bootstrap-sass/component-animations"
//@import "vendor/bootstrap-sass/glyphicons"
//@import "vendor/bootstrap-sass/dropdowns"
//@import "vendor/bootstrap-sass/button-groups"
//@import "vendor/bootstrap-sass/input-groups"
//@import "vendor/bootstrap-sass/navs"
//@import "vendor/bootstrap-sass/navbar"
//@import "vendor/bootstrap-sass/breadcrumbs"
//@import "vendor/bootstrap-sass/pagination"
//@import "vendor/bootstrap-sass/pager"
//@import "vendor/bootstrap-sass/labels"
//@import "vendor/bootstrap-sass/badges"
//@import "vendor/bootstrap-sass/jumbotron"
//@import "vendor/bootstrap-sass/thumbnails"
//@import "vendor/bootstrap-sass/alerts"
//@import "vendor/bootstrap-sass/progress-bars"
//@import "vendor/bootstrap-sass/media"
//@import "vendor/bootstrap-sass/list-group"
//@import "vendor/bootstrap-sass/panels"
//@import "vendor/bootstrap-sass/wells"
//@import "vendor/bootstrap-sass/close"
// Components w/ JavaScript
//@import "vendor/bootstrap-sass/tooltip"
//@import "vendor/bootstrap-sass/popovers"
//@import "vendor/bootstrap-sass/carousel"
//Utility classes
//@import "vendor/bootstrap-sass/utilities"
//@import "vendor/bootstrap-sass/responsive-utilities"
//@import "vendor/bootstrap-sass/modals"

#content
	padding: 20px

@include web-font("Open+Sans",400)<?php }} ?>