<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_turquoise.scss" */ ?>
<?php /*%%SmartyHeaderCode:1695053a035bc97b274-46310050%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '58093af306797b5fec3ea975d39d22d38e878543' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_turquoise.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1695053a035bc97b274-46310050',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc97d934_37167409',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc97d934_37167409')) {function content_53a035bc97d934_37167409($_smarty_tpl) {?>// Turquoise 9
// -------------------------

$turquoise:             #48E8C8 !default;
$turquoiseDark:         #00997A !default;
$turquoiseDarker:       #006652 !default;
$turquoiseLight:        #6BE8CF !default;
$turquoiseLighter:      #8EE8D6 !default;
$turquoiseDull:         #52CCB4 !default;
$turquoiseDuller:       #69B8A8 !default;
$turquoiseBright:       #00FFCC !default;
$turquoiseBrighter:     #80FFE6 !default;

// * * * Premium * * *
// -------------------------


//Dulux 3

$turquoiseTeal:   		 #01969c !default;
$turquoiseDawn:   		 #35AFBA !default;
$turquoiseCrystal:   	 #2BA8A8 !default;<?php }} ?>