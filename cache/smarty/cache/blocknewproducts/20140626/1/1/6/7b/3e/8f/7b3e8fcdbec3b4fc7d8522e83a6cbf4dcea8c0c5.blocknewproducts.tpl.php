<?php /*%%SmartyHeaderCode:937253abd265a7cf77-42541398%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b3e8fcdbec3b4fc7d8522e83a6cbf4dcea8c0c5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\modules\\blocknewproducts\\blocknewproducts.tpl',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '937253abd265a7cf77-42541398',
  'variables' => 
  array (
    'link' => 0,
    'new_products' => 0,
    'newproduct' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53abd265b45142_59059155',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53abd265b45142_59059155')) {function content_53abd265b45142_59059155($_smarty_tpl) {?><!-- MODULE Block new products -->
<div id="new-products_block_right" class="block products_block">
	<h4 class="title_block">
    	<a href="http://localhost/furfashiongroupbarcelona/es/nuevos-productos" title="Novedades">Novedades</a>
    </h4>
    <div class="block_content products-block">
                    <ul class="products">
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/5-printed-summer-dress.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/12-small_default/printed-summer-dress.jpg" alt="Printed Summer Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress">Printed Summer Dress</a>
                            </h5>
                        	<p class="product-description">Long printed dress with thin adjustable straps. V-neckline and wiring...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	35,07 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/6-printed-summer-dress.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/16-small_default/printed-summer-dress.jpg" alt="Printed Summer Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress">Printed Summer Dress</a>
                            </h5>
                        	<p class="product-description">Sleeveless knee-length chiffon dress. V-neckline with elastic under the...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	36,91 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/7-printed-chiffon-dress.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/20-small_default/printed-chiffon-dress.jpg" alt="Printed Chiffon Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/summer-dresses/7-printed-chiffon-dress.html" title="Printed Chiffon Dress">Printed Chiffon Dress</a>
                            </h5>
                        	<p class="product-description">Printed chiffon knee length dress with tank straps. Deep v-neckline.</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	19,85 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/casual-dresses/3-printed-dress.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/8-small_default/printed-dress.jpg" alt="Printed Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/casual-dresses/3-printed-dress.html" title="Printed Dress">Printed Dress</a>
                            </h5>
                        	<p class="product-description">100% cotton double printed dress. Black and white striped top and orange...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	31,46 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/evening-dresses/4-printed-dress.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/10-small_default/printed-dress.jpg" alt="Printed Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/evening-dresses/4-printed-dress.html" title="Printed Dress">Printed Dress</a>
                            </h5>
                        	<p class="product-description">Printed evening dress with straight sleeves with black thin waist belt...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	61,70 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/blouses/2-blouse.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/7-small_default/blouse.jpg" alt="Blouse" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/blouses/2-blouse.html" title="Blouse">Blouse</a>
                            </h5>
                        	<p class="product-description">Short sleeved blouse with feminine draped sleeve detail.</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	32,67 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://localhost/furfashiongroupbarcelona/es/tshirts/1-faded-short-sleeve-tshirts.html" title=""><img class="replace-2x img-responsive" src="http://localhost/furfashiongroupbarcelona/1-small_default/faded-short-sleeve-tshirts.jpg" alt="Faded Short Sleeve T-shirts" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://localhost/furfashiongroupbarcelona/es/tshirts/1-faded-short-sleeve-tshirts.html" title="Faded Short Sleeve T-shirts">Faded Short Sleeve T-shirts</a>
                            </h5>
                        	<p class="product-description">Faded short sleeve t-shirt with high neckline. Soft and stretchy...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	19,98 €                                        </span>
                                    </div>
                                                                                    </div>
                    </li>
                            </ul>
            <div>
                <a href="http://localhost/furfashiongroupbarcelona/es/nuevos-productos" title="Todas los nuevos productos" class="btn btn-default button button-small"><span>Todas los nuevos productos<i class="icon-chevron-right right"></i></span></a>
            </div>
            </div>
</div>
<!-- /MODULE Block new products --><?php }} ?>