<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_jumbotron.scss" */ ?>
<?php /*%%SmartyHeaderCode:992153a035b4b15082-87232516%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eadea2f71208ca4565e7eba1264966c200e095f3' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_jumbotron.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '992153a035b4b15082-87232516',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4b1a836_89384074',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4b1a836_89384074')) {function content_53a035b4b1a836_89384074($_smarty_tpl) {?>//
// Jumbotron
// --------------------------------------------------


.jumbotron {
  padding: $jumbotron-padding;
  margin-bottom: $jumbotron-padding;
  font-size: ($font-size-base * 1.5);
  font-weight: 200;
  line-height: ($line-height-base * 1.5);
  color: $jumbotron-color;
  background-color: $jumbotron-bg;

  h1 {
    line-height: 1;
    color: $jumbotron-heading-color;
  }
  p {
    line-height: 1.4;
  }

  .container & {
    border-radius: $border-radius-large; // Only round corners at higher resolutions if contained in a container
  }

  @media screen and (min-width: $screen-tablet) {
    padding-top:    ($jumbotron-padding * 1.6);
    padding-bottom: ($jumbotron-padding * 1.6);

    .container & {
      padding-left:  ($jumbotron-padding * 2);
      padding-right: ($jumbotron-padding * 2);
    }

    h1 {
      font-size: ($font-size-base * 4.5);
    }
  }
}
<?php }} ?>