<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_labels.scss" */ ?>
<?php /*%%SmartyHeaderCode:3235853a035b4b27398-92781853%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '136b3771a737d53113a70d5c9ef013e5c4a93ede' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_labels.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3235853a035b4b27398-92781853',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4b2c204_08923551',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4b2c204_08923551')) {function content_53a035b4b2c204_08923551($_smarty_tpl) {?>//
// Labels
// --------------------------------------------------

.label {
  display: inline;
  padding: .2em .6em .3em;
  font-size: 100%;
  font-weight: bold;
  line-height: 1;
  color: $label-color;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: 0;

  // Add hover effects, but only for links
  &[href] {
    &:hover,
    &:focus {
      color: $label-link-hover-color;
      text-decoration: none;
      cursor: pointer;
    }
  }

  // Empty labels collapse automatically (not available in IE8)
  &:empty {
    display: none;
  }
}

// Colors
// Contextual variations (linked labels get darker on :hover)

.label-default {
  @include label-variant($label-default-bg);
}

.label-primary {
  @include label-variant($label-primary-bg);
}

.label-success {
  @include label-variant($label-success-bg);
  border:1px solid $label-success-border;
}

.label-info {
  @include label-variant($label-info-bg);
   border:1px solid $label-info-border;
}

.label-warning {
  @include label-variant($label-warning-bg);
  border:1px solid $label-warning-border;
}

.label-danger {
  @include label-variant($label-danger-bg);
  border:1px solid $label-danger-border;
}
<?php }} ?>