<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_browns.scss" */ ?>
<?php /*%%SmartyHeaderCode:2608353a035bc8dff84-57670609%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1e96401cdbec975595060d99094c76b4d533d283' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_browns.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2608353a035bc8dff84-57670609',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc8e2837_57092388',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc8e2837_57092388')) {function content_53a035bc8e2837_57092388($_smarty_tpl) {?>// Browns 21
// -------------------------

//Defaults

$brown:             	#663300 !default;
$brownDark:         	#472400 !default;
$brownDarker:       	#331A00 !default;
$brownLight:        	#754719 !default;
$brownLighter:      	#855C33 !default;
$brownDull:         	#664728 !default;
$brownDuller:       	#5E4C3A !default;
$brownBright:       	#993300 !default;
$brownBrighter:     	#A34719 !default;

//Variations

$brownBeaver:        	#9f8170 !default;
$brownBistre:        	#3d2b1f !default;
$brownBole:        		#79443b !default;
$brownBronze:        	#cd7f32 !default;
$brownBurnt:        	#8a3324 !default;
$brownCamel:        	#c19a6b !default;
$brownChestnut:        	#954535 !default;
$brownChocolate:        #7b3f00 !default;
$brownCoffee:        	#6f4e37 !default;
$brownCopper:        	#b87333 !default;
$brownMahogany:        	#c04000 !default;
$brownSepia:        	#704214 !default;


// * * * Premium * * *
// -------------------------


//Dulux 8

$brownCaramel:        	#AC9D8E !default;
$brownPraline:        	#9F8E82 !default;
$brownTruffle:        	#8E796B !default;
$brownExotic:        	#8E766C !default;
$brownBitter:        	#82695F !default;
$brownCocoa:        	#7E6660 !default;
$brownMocha:        	#7F6968 !default;
$brownClay:        		#D7C3B8 !default;<?php }} ?>