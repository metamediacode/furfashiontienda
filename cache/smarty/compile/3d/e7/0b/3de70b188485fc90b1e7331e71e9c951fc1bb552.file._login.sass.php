<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\controllers\_login.sass" */ ?>
<?php /*%%SmartyHeaderCode:2161153a035bc869ee8-86805624%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3de70b188485fc90b1e7331e71e9c951fc1bb552' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\controllers\\_login.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2161153a035bc869ee8-86805624',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc86d6e5_25148828',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc86d6e5_25148828')) {function content_53a035bc86d6e5_25148828($_smarty_tpl) {?>#login
	height: 100%
	background-color: #092756
	@include background(radial-gradient(color-stops(rgba(104,128,138,.4) 10%,rgba(138,114,76,0) 40%), top left),linear-gradient(top,rgba(57,173,219,.25),rgba(42,60,87,.4)),linear-gradient(left top,#670D10,#092756))

#login-header
	padding-top: 40px
	margin-bottom: 30px
	color: white
	h1, h4
		margin: 0
		padding: 0

#login-panel
	margin: 0 auto
	width: 420px
	@media (max-width: $screen-phone)
		width: 90%
	.panel
		@include box-shadow(rgba(white,0.45) 0 0 0 6px)
	.panel-footer
		height: inherit
		margin: 0 -20px -20px
	.flip-container
		height: 370px
		@include perspective(1000px)
		//@include animate(fadeInDown, 0.3s)
		&.flip .flipper
			@include rotateY(180deg)
	.flipper
		position: relative
		@include transition-duration(0.6s)
		@include transform-style
	.front, .back
		width: 100%
		padding: 20px
		position: absolute
		top: 0
		@include backface-visibility(hidden)
		@include left(0)
	.front
		z-index: 2
	.back
		@include rotateY(180deg)
	#remind-me
		margin-top: 0

#login-footer
	a
		color: #A0AAB5<?php }} ?>