<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_yellows.scss" */ ?>
<?php /*%%SmartyHeaderCode:1501253a035bc996347-53681629%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '016c7e7961ec6e580fcd35f4b98d2f152982dc45' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_yellows.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1501253a035bc996347-53681629',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc9984a2_16918360',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc9984a2_16918360')) {function content_53a035bc9984a2_16918360($_smarty_tpl) {?>// Yellow 17
// -------------------------

//Defaults

$yellow:               	#FFCC00 !default;
$yellowDark:            #806600 !default;
$yellowDarker:          #332900 !default;
$yellowLight:           #FFDB4D !default;
$yellowLighter:         #FFEB99 !default;
$yellowDull:           	#CFB036 !default;
$yellowDuller:         	#B39F52 !default;
$yellowBright:         	#FFFF00 !default;
$yellowBrighter:        #FFFF66 !default;


//Variations

$yellowBuff:        	#f0dc82 !default;
$yellowGold:        	#ffd700 !default;
$yellowGoldmetal:       #d4af37 !default;
$yellowJasmine:        	#f8de7e !default;
$yellowMaize:        	#fbec5d !default;
$yellowSaffron:        	#f4c430 !default;
$yellowSunglow:        	#ffcc33 !default;
$yellowLight:        	#fdfc8f !default;


// * * * Premium * * *
// -------------------------


//Dulux 13

$yellowAboutme:         #ffcc33 !default;
$yellowAim:          	#fcd20b !default;
$yellowPharaoh:   	    #FDC46B !default;
$yellowHavana:			#FBBB5C !default;
$yellowSunny:			#FFD251 !default;
$yellowDelhi:			#FFC924 !default;
$yellowSpring:			#FFCD59 !default;
$yellowSunflower: 		#FFC43C !default;
$yellowGlaze:			#FFC054 !default;
$yellowBreeze:			#F0D458 !default;
$yellowEaster:			#FECF51 !default;
$yellowLemon:			#FFD865 !default;
$yellowButtercup:		#F9C833 !default;










<?php }} ?>