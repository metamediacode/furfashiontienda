<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_thumbnails.scss" */ ?>
<?php /*%%SmartyHeaderCode:2880453a035b4cd9210-97067007%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bdc9acc05ec53a384b43c8bcaae819a40cd73657' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_thumbnails.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2880453a035b4cd9210-97067007',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4cdce41_51159172',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4cdce41_51159172')) {function content_53a035b4cdce41_51159172($_smarty_tpl) {?>//
// Thumbnails
// --------------------------------------------------


// Mixin and adjust the regular image class
.thumbnail {
  @extend .img-thumbnail;
  display: block; // Override the inline-block from `.img-thumbnail`

  > img {
    @include img-responsive();
  }
}


// Add a hover state for linked versions only
a.thumbnail:hover,
a.thumbnail:focus {
  border-color: $link-color;
}

// Images and captions
.thumbnail > img {
  margin-left: auto;
  margin-right: auto;
}
.thumbnail .caption {
  padding: $thumbnail-caption-padding;
  color: $thumbnail-caption-color;
}
<?php }} ?>