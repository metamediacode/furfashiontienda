<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_purples.scss" */ ?>
<?php /*%%SmartyHeaderCode:1186753a035bc950f53-67094934%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a73dfc6842bb04a32c3408402f6860c4574d904e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_purples.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1186753a035bc950f53-67094934',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc952f40_22423562',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc952f40_22423562')) {function content_53a035bc952f40_22423562($_smarty_tpl) {?>// Purples 19
// -------------------------


//Defaults

$purple:             	#9966FF !default;
$purpleDark:         	#5C3D99 !default;
$purpleDarker:       	#2E1F4C !default;
$purpleLight:        	#AD85FF !default;
$purpleLighter:      	#C2A3FF !default;
$purpleDull:         	#957BC7 !default;
$purpleDuller:       	#9589AD !default;
$purpleBright:       	#9933FF !default;
$purpleBrighter:     	#AD5CFF !default;

//Variations

$purpleAmethyst:        #9966cc !default;
$purpleCerise:        	#de3163 !default;
$purpleFandango:        #b53389 !default;
$purpleHeliotrope:      #df73ff !default;
$purpleLavendar:       	#b57edc !default;
$purpleMagenta:        	#ff0090 !default;
$purpleOrchid:        	#da70d6 !default;
$purplePlum:        	#8e4585 !default;

//Brands

$purpleHeroku:        	#6567a5 !default;
$purpleYahoo:        	#720e9e !default;


// * * * Premium * * *
// -------------------------


//Dulux 8

$purpleRose:    		 #865873 !default;
$purpleBlush:   		 #864E66 !default;
$purpleOpera: 	 		 #B977A9 !default;
$purpleVerona: 	 		 #A77AAC !default;
$purpleSpring: 	 		 #A389B8 !default;
$purpleBlossom:  		 #8A7AA2 !default;
$purpleShowers:  		 #7571A4 !default;
$purpleLilac:  			 #797FA8 !default;


<?php }} ?>