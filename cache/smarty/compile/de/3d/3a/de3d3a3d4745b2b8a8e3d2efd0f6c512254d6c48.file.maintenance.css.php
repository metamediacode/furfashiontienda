<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:52
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\css\maintenance.css" */ ?>
<?php /*%%SmartyHeaderCode:3001353a035b01e1612-68773749%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de3d3a3d4745b2b8a8e3d2efd0f6c512254d6c48' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\css\\maintenance.css',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3001353a035b01e1612-68773749',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b01e6d73_04174312',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b01e6d73_04174312')) {function content_53a035b01e6d73_04174312($_smarty_tpl) {?>/*********************************************************************
					Maintenance Page Styles
**********************************************************************/
#maintenance {
  margin: 50px 0 0 0; }
  @media (min-width: 1200px) {
    #maintenance {
      margin: 126px 0 0 0;
      padding: 91px 48px 365px 297px;
      background: url(../img/bg_maintenance.png) no-repeat; } }
  #maintenance .logo {
    margin: 0 0 31px 0; }
  #maintenance h1 {
    font: 600 28px/34px "Open Sans", sans-serif;
    color: #333333;
    text-transform: uppercase;
    border-bottom: 1px solid #d6d4d4;
    padding: 0 0 14px 0;
    margin: 0 0 19px 0; }
  #maintenance #message {
    font: 600 16px/31px "Open Sans", sans-serif;
    padding: 0 0 0 18px;
    color: #555454;
    text-transform: uppercase; }

@media only screen and (min-width: 1200px) {
  .container {
    padding-left: 0;
    padding-right: 0; } }
<?php }} ?>