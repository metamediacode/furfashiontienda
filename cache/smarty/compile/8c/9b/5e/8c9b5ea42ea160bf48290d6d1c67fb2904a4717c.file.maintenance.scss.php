<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\maintenance.scss" */ ?>
<?php /*%%SmartyHeaderCode:566453a035b51416d4-50197561%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8c9b5ea42ea160bf48290d6d1c67fb2904a4717c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\maintenance.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '566453a035b51416d4-50197561',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b51450e8_61958539',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b51450e8_61958539')) {function content_53a035b51450e8_61958539($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
/*********************************************************************
					Maintenance Page Styles
**********************************************************************/
#maintenance {
	margin: 50px 0 0 0;
	@media (min-width: $screen-lg) { // > 1200 
		margin: 126px 0 0 0;
		padding: 91px 48px 365px 297px;
		background: url(../img/bg_maintenance.png) no-repeat;
	}
	.logo {
		margin: 0 0 31px 0;	
	}
	h1 {
		font: 600 28px/34px $font-custom;
		color: $base-text-color;
		text-transform: uppercase;
		border-bottom: 1px solid $base-border-color;
		padding: 0 0 14px 0;
		margin: 0 0 19px 0;
	}
	#message {
		font: 600 16px/31px $font-custom;
		padding: 0 0 0 18px;
		color: #555454;
		text-transform: uppercase;	
	}
}
@media only screen and (min-width: 1200px) {
	.container { 
		padding-left: 0;
		padding-right: 0;
	}
}<?php }} ?>