<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\modules\favoriteproducts\favoriteproducts.scss" */ ?>
<?php /*%%SmartyHeaderCode:1058653a035b52e4163-42487688%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad2e05656f94939128af52bd1f6ce31dfa661620' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\modules\\favoriteproducts\\favoriteproducts.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1058653a035b52e4163-42487688',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b52e9486_07717418',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b52e9486_07717418')) {function content_53a035b52e9486_07717418($_smarty_tpl) {?>@import "compass";
@import "theme_variables";

#favoriteproducts_block_account{
	.favoriteproduct {
		position:relative;
		margin-bottom: 14px;
		padding: 12px 8px;
		border: 1px solid $base-border-color;
		@include border-radius(3px);
		a.product_img_link {
			border: 1px solid $base-border-color;
			display: block;
			float: left;
			margin-right: 14px;
			overflow: hidden;
			position: relative;
		}
		p.product_desc {
			line-height: 16px;
			overflow: hidden;
			padding: 0;
		}
		.remove {
			position:absolute;
			top:10px;
			right:10px;
			.icon {
				cursor:pointer;
			}
		}
	}
} 

/* lnk fiche produit */
#usefull_link_block {
	li{
		&#favoriteproducts_block_extra_add, 
		&#favoriteproducts_block_extra_remove,
		&#favoriteproducts_block_extra_added,
		&#favoriteproducts_block_extra_removed{
			padding-left: 30px;
			cursor: pointer;
		}
		&#favoriteproducts_block_extra_added {
			display: none;
		}
		&#favoriteproducts_block_extra_removed {
			display: none;
		}
	}
}<?php }} ?>