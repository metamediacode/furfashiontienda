<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_breadcrumbs.scss" */ ?>
<?php /*%%SmartyHeaderCode:193353a035b494b004-26408237%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '33308199125e8db8a90dcd5b9a98ad737dc07b8f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_breadcrumbs.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '193353a035b494b004-26408237',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b494ef35_78530522',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b494ef35_78530522')) {function content_53a035b494ef35_78530522($_smarty_tpl) {?>//
// Breadcrumbs
// --------------------------------------------------


.breadcrumb {
  padding: 8px 15px;
  margin-bottom: $line-height-computed;
  list-style: none;
  background-color: $breadcrumb-bg;
  border-radius: $border-radius-base;
  > li {
    display: inline-block;
    &+li:before {
      content: "/\00a0"; // Unicode space added since inline-block means non-collapsing white-space
      padding: 0 5px;
      color: $breadcrumb-color;
    }
  }
  > .active {
    color: $breadcrumb-active-color;
  }
}
<?php }} ?>