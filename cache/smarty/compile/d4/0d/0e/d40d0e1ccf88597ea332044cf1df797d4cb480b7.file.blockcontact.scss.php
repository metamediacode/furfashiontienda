<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\modules\blockcontact\blockcontact.scss" */ ?>
<?php /*%%SmartyHeaderCode:2370153a035b51bf9a3-83965392%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd40d0e1ccf88597ea332044cf1df797d4cb480b7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\modules\\blockcontact\\blockcontact.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2370153a035b51bf9a3-83965392',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b51c6a11_18555113',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b51c6a11_18555113')) {function content_53a035b51c6a11_18555113($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
.shop-phone {
	float: left;
	padding: 5px 0 10px;
	line-height:18px;
	@media (max-width: $screen-xs-max) {
		display: none;
	}
	i {
		font-size: 21px;
		line-height: 21px;
		color: $light-text-color;
		padding-right: 7px;
	}
	strong {
		color: $light-text-color;
	}
}
#contact-link {
	float: right;
	border-left: 1px solid #515151;
	@media (max-width: $screen-xs - 1) {
		width: 25%;
		text-align: center;
	}													
	a {
		display: block;
		color: $light-text-color;
		font-weight: bold;
		padding: 8px 10px 11px 10px;
		text-shadow: 1px 1px rgba(0,0,0,0.2);
		cursor: pointer;
		line-height:18px;
		@media (max-width: $screen-xs - 1) {
			font-size: 11px;
			padding-left: 5px;
			padding-right: 5px;
		}
		&:hover, &.active {
			background: #2b2b2b;
		}
	}
}
#contact_block {
	@media (max-width: $screen-xs-max) { // max 767px
		margin-bottom: 20px;
	}
	.label {
		display: none
	}
	.block_content {
		color: #888888;
	}
	p {
		margin-bottom: 4px;
	}
	p.tel {
		font: 400 17px/21px $font-family;
		color: $base-text-color;
		margin-bottom: 6px;
		i {
			font-size: 25px;
			vertical-align: -2px; 
			padding-right: 10px;
		}
	}
}
<?php }} ?>