<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\_font-awesome.scss" */ ?>
<?php /*%%SmartyHeaderCode:743453a035b5495536-46824462%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2297120b9a40786111cb1d1653fad4afbd8f160d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\_font-awesome.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '743453a035b5495536-46824462',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b54970f2_46658300',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b54970f2_46658300')) {function content_53a035b54970f2_46658300($_smarty_tpl) {?>/*!
 *  Font Awesome 3.2.1
 *  the iconic font designed for Bootstrap
 *  ------------------------------------------------------------------------------
 *  The full suite of pictographic icons, examples, and documentation can be
 *  found at http://fontawesome.io.  Stay up to date on Twitter at
 *  http://twitter.com/fontawesome.
 *
 *  License
 *  ------------------------------------------------------------------------------
 *  - The Font Awesome font is licensed under SIL OFL 1.1 -
 *    http://scripts.sil.org/OFL
 *  - Font Awesome CSS, LESS, and SASS files are licensed under MIT License -
 *    http://opensource.org/licenses/mit-license.html
 *  - Font Awesome documentation licensed under CC BY 3.0 -
 *    http://creativecommons.org/licenses/by/3.0/
 *  - Attribution is no longer required in Font Awesome 3.0, but much appreciated:
 *    "Font Awesome by Dave Gandy - http://fontawesome.io"
 *
 *  Author - Dave Gandy
 *  ------------------------------------------------------------------------------
 *  Email: dave@fontawesome.io
 *  Twitter: http://twitter.com/davegandy
 *  Work: Lead Product Designer @ Kyruus - http://kyruus.com
 */

@import "font-awesome/variables";
@import "font-awesome/mixins";
@import "font-awesome/path";
@import "font-awesome/core";
@import "font-awesome/bootstrap";
@import "font-awesome/extras";
@import "font-awesome/icons";
<?php }} ?>