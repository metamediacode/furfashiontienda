<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_wells.scss" */ ?>
<?php /*%%SmartyHeaderCode:164153a035b4d461e8-11286747%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '439ff28d7825cf5d955573b7142fe688ee096faf' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_wells.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164153a035b4d461e8-11286747',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4d49007_43879512',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4d49007_43879512')) {function content_53a035b4d49007_43879512($_smarty_tpl) {?>//
// Wells
// --------------------------------------------------


// Base class
.well {
  min-height: 20px;
  padding: 19px;
  margin-bottom: 20px;
  background-color: $well-bg;
  border: 1px solid darken($well-bg, 7%);
  border-radius: $border-radius-base;
  @include box-shadow(inset 0 1px 1px rgba(0,0,0,.05));
  blockquote {
    border-color: #ddd;
    border-color: rgba(0,0,0,.15);
  }
}

// Sizes
.well-lg {
  padding: 24px;
  border-radius: $border-radius-large;
}
.well-sm {
  padding: 9px;
  border-radius: $border-radius-small;
}
<?php }} ?>