<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\modules\blocktags\blocktags.scss" */ ?>
<?php /*%%SmartyHeaderCode:717053a035b5252822-10502360%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5097cb78ba8ec14685fa32a6852264064694a12b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\modules\\blocktags\\blocktags.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '717053a035b5252822-10502360',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b5255977_35878189',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b5255977_35878189')) {function content_53a035b5255977_35878189($_smarty_tpl) {?>@import '../../_theme_variables';
.tags_block {
	
	.block_content {
		overflow:hidden;
		
		a {
			display:inline-block;
			font-size:13px;
			line-height:16px;
			font-weight:bold;
			padding:4px 9px 5px 9px;
			border:1px solid $base-border-color;
			float:left;
			margin:0 3px 3px 0;
			
			&:hover {
				color:$base-text-color;
				background:#f6f6f6;
			}
		}
	}
}<?php }} ?>