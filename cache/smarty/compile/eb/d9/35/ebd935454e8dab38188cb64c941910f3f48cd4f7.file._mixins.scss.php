<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:05
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\vendor\font-awesome\_mixins.scss" */ ?>
<?php /*%%SmartyHeaderCode:273353a035bd7d4740-88885478%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebd935454e8dab38188cb64c941910f3f48cd4f7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\vendor\\font-awesome\\_mixins.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '273353a035bd7d4740-88885478',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bd7d71e1_87887585',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bd7d71e1_87887585')) {function content_53a035bd7d71e1_87887585($_smarty_tpl) {?>// Mixins
// --------------------------

@mixin fa-icon-rotate($degrees, $rotation) {
  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=$rotation);
  -webkit-transform: rotate($degrees);
     -moz-transform: rotate($degrees);
      -ms-transform: rotate($degrees);
       -o-transform: rotate($degrees);
          transform: rotate($degrees);
}

@mixin fa-icon-flip($horiz, $vert, $rotation) {
  filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=$rotation);
  -webkit-transform: scale($horiz, $vert);
     -moz-transform: scale($horiz, $vert);
      -ms-transform: scale($horiz, $vert);
       -o-transform: scale($horiz, $vert);
          transform: scale($horiz, $vert);
}
<?php }} ?>