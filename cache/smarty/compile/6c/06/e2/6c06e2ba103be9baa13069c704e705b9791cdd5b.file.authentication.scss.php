<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\authentication.scss" */ ?>
<?php /*%%SmartyHeaderCode:933653a035b4835155-04598221%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6c06e2ba103be9baa13069c704e705b9791cdd5b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\authentication.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '933653a035b4835155-04598221',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b483a7a1_25789981',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b483a7a1_25789981')) {function content_53a035b483a7a1_25789981($_smarty_tpl) {?>@import "compass";
@import "theme_variables";
/**********************************************************
				Authentication Styles
**********************************************************/
#account-creation_form .id_state,
#account-creation_form .dni,
#account-creation_form .postcode {
	display: none;
}

#create-account_form {
	min-height: 297px;
	p {
		margin-bottom: 8px;
	}
	.form-group {
		margin: 0 0 20px 0;
	}
}

#login_form {
	min-height: 297px;
	.form-group {
		margin: 0 0 3px 0;
		&.lost_password {
			margin: 14px 0 15px 0;
			a {
				text-decoration: underline;
				&:hover {
					text-decoration: none;
				}	
			}
		}
	}
}

#login_form, #create-account_form {
	.form-control {
		max-width: 271px;	
	}
}<?php }} ?>