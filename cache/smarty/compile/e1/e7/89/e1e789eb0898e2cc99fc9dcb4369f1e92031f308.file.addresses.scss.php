<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\addresses.scss" */ ?>
<?php /*%%SmartyHeaderCode:1276353a035b4824579-92262114%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e1e789eb0898e2cc99fc9dcb4369f1e92031f308' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\addresses.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1276353a035b4824579-92262114',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b4828f89_03261204',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b4828f89_03261204')) {function content_53a035b4828f89_03261204($_smarty_tpl) {?>@import "compass";
@import "theme_variables";

/*******************************************************
				Addresses Styles
********************************************************/
#addresses {
	#center_column {
		.page-heading {
			margin: 0 0 22px;	
		}
		p {
			margin: 0 0 13px;

			&.p-indent {
				margin: 0 0 27px;
			}	
		}
	}
}

.address {
	li {
		&.address_update {
			margin: 14px 0 6px 0;
			a {
				margin: 0 10px 0 0;
			}
		}
	}
}<?php }} ?>