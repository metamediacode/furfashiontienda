<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:56
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\bootstrap_lib\_badges.scss" */ ?>
<?php /*%%SmartyHeaderCode:413253a035b4939b77-21441964%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ec45616aac18f4e78c50a01c3c9a168ca9a754d8' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\bootstrap_lib\\_badges.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '413253a035b4939b77-21441964',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b493e593_89484424',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b493e593_89484424')) {function content_53a035b493e593_89484424($_smarty_tpl) {?>//
// Badges
// --------------------------------------------------


// Base classes
.badge {
  display: inline-block;
  min-width: 10px;
  padding: 3px 7px;
  font-size: $font-size-small;
  font-weight: $badge-font-weight;
  color: $badge-color;
  line-height: $badge-line-height;
  vertical-align: baseline;
  white-space: nowrap;
  text-align: center;
  background-color: $badge-bg;
  border-radius: $badge-border-radius;

  // Empty badges collapse automatically (not available in IE8)
  &:empty {
    display: none;
  }
}

// Hover state, but only for links
a.badge {
  &:hover,
  &:focus {
    color: $badge-link-hover-color;
    text-decoration: none;
    cursor: pointer;
  }
}

// Quick fix for labels/badges in buttons
.btn .badge {
  position: relative;
  top: -1px;
}

// Account for counters in navs
a.list-group-item.active > .badge,
.nav-pills > .active > a > .badge {
  color: $badge-active-color;
  background-color: $badge-active-bg;
}
.nav-pills > li > a > .badge {
  margin-left: 3px;
}
<?php }} ?>