<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:57
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\sass\scenes.scss" */ ?>
<?php /*%%SmartyHeaderCode:383853a035b54545d2-46682276%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb630be003902fa12c9617cc6804c457313fbb49' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\sass\\scenes.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '383853a035b54545d2-46682276',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b545c725_70134232',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b545c725_70134232')) {function content_53a035b545c725_70134232($_smarty_tpl) {?>@import "compass";
@import "theme_variables";

/********************************************************
			Scenes Styles
********************************************************/

#scenes .popover-button span{
	display: block;
	font-size: 28px;
	text-align: center;
	&:before {
		content: "\f0fe";
		font-family: $font-icon;
		display: block;	
		vertical-align: 5px;
	}
}

.thumbs_banner{
	margin: 10px auto;
	padding: 0;
	height: 62px !important;
	width: 100%;
	border-top: 1px solid $base-border-color;
	border-bottom: 1px solid $base-border-color;
	.space-keeper{
		width: 21px;
		float: left;
		display: block;
		height: 100%;
		a.prev{
			display: none;
			width: 21px;
			background: $base-box-bg;
			text-decoration: none;
			color: $base-text-color;
			&:before {
				font-family: $font-icon;
				content: "\f053";
				vertical-align: middle;
				padding-top: 35px;
				padding-left: 5px;
			}
		}
		a.next {
			float: left;
			display: block;
			width: 21px;
			background: $base-box-bg;
			text-decoration: none;
			color: $base-text-color;
			&:before {
				font-family: $font-icon;
				content: "\f054";
				vertical-align: middle;
				padding-top: 35px;
				padding-left: 5px;
			}
		}	
	}
}

#scenes_list{
	overflow: hidden;
	float: left;
	width: 828px;
	ul{
		list-style-type: none;
	}
	li{
		float: left;
	}
	a{
		display: block;
	}
}

#scenes {
	a.popover-button {
		display: block;
		position: absolute;
		text-decoration: none;
	}
	.popover {
		@include border-radius(0);
		border-color: $base-border-color;
	}
	.product-image-container, 
	.product-name {
		margin-bottom: 15px;	
	}
	div.description {
		margin-bottom: 15px;	
	}
	.button-container {
		margin-bottom: 15px;
		a {
			text-decoration: none;
		}
	}
	.price {
		margin-bottom: 10px;	
	}
	@media  (max-width: $screen-md-max) { // max 1199px 
		display: none;	
	}
}
<?php }} ?>