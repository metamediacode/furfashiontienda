<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\vendor\bootstrap-rtl-sass\_pagination-rtl.sass" */ ?>
<?php /*%%SmartyHeaderCode:338053a035bccc6fe1-20810075%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7d8a2df252ad58e9fd95fdeb4ce540bba6e22f70' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\vendor\\bootstrap-rtl-sass\\_pagination-rtl.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '338053a035bccc6fe1-20810075',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bccc8d56_51855693',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bccc8d56_51855693')) {function content_53a035bccc8d56_51855693($_smarty_tpl) {?>//
// RTL Pagination (multiple pages)
// --------------------------------------------------
.pagination
	padding-right: 0
	> li
		> a,
		> span
			float: right // Collapse white-space
			margin-right: -1px
			margin-left: 0px
		&:first-child
			> a,
			> span
				margin-left: 0
				@include border-right-radius($border-radius-base)
				@include border-left-radius(0)
		&:last-child
			> a,
			> span
				margin-right: -1px
				@include border-left-radius($border-radius-base)
				@include border-right-radius(0)<?php }} ?>