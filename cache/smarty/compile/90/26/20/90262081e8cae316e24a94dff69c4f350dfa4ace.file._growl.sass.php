<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\partials\_growl.sass" */ ?>
<?php /*%%SmartyHeaderCode:2233753a035bca5d702-46490647%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '90262081e8cae316e24a94dff69c4f350dfa4ace' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\partials\\_growl.sass',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2233753a035bca5d702-46490647',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'namespace' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bca70b77_52174939',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bca70b77_52174939')) {function content_53a035bca70b77_52174939($_smarty_tpl) {?>/*
 * jQuery Growl
 * Copyright 2013 Kevin Sylvestre
 * 1.1.4
 */

$namespace: "growl"
$duration: 0.4s

#growls
	z-index: 50000
	position: fixed
	
	&.default
		top: 100px
		@include right(10px)
	&.tl
		top: 10px
		@include left(10px)
	&.tr
		top: 10px
		@include right(10px)
	&.bl
		bottom: 10px
		@include left(10px)
	&.br
		bottom: 10px
		@include right(10px)

.growl
	opacity: 1
	position: relative
	@include border-radius(4px)
	@include transition(all $duration ease-in-out)

	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-incoming
		opacity: 0.0

	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-outgoing
		opacity: 0.0

	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-small
		width: 200px
		padding: 5px
		margin: 5px
	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-medium
		width: 250px
		padding: 10px
		margin: 10px
	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-large
		width: 300px
		padding: 15px
		margin: 15px

	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-default
		color: #FFF
		background: gray
	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-error
		color: white
		background: rgba($brand-danger,0.8)
	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-notice
		color: white
		background: rgba($brand-success,0.8)
	&.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-warning
		color: white
		background: rgba($brand-warning,0.8)
	.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-close
		cursor: pointer
		font-size: 14px
		line-height: 18px
		font-weight: normal
		font-family: helvetica, verdana, sans-serif
		@include float(right)
	.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-title
		font-size: 18px
		line-height: 24px
	.#<?php echo $_smarty_tpl->tpl_vars['namespace']->value;?>
-message
		font-size: 14px
		line-height: 18px<?php }} ?>