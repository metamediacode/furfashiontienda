<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_reds.scss" */ ?>
<?php /*%%SmartyHeaderCode:997653a035bc95f503-94527285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '62fd0acc5808757ac7b54b2b4e33363dc864a6ac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_reds.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '997653a035bc95f503-94527285',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc9617a0_88258297',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc9617a0_88258297')) {function content_53a035bc9617a0_88258297($_smarty_tpl) {?>// Reds 21
// -------------------------

//Defaults

$red:                 	#D42C2C !default;
$redDark:              	#7A0000 !default;
$redDarker:            	#3D0000 !default;
$redLight:              #DB4D4D !default;
$redLighter:            #F0B2B2 !default;
$redDull:            	#993333 !default;
$redDuller:            	#824D4D !default;
$redBright:            	#FF0000 !default;
$redBrighter:           #FF6E6E !default;

//Variations

$redAuburn:        		#a52a2a !default;
$redBurgundy:        	#9f1d35 !default;
$redChestnut:        	#9a1a00 !default;
$redCrimson:        	#dc143c !default;
$redBrick:        		#b22222 !default;
$redWood:        		#ab4e52 !default;
$redRust:        		#b7410e !default;
$redWine:        		#722f37 !default;

//Brands

$redGoogle:        		#db4a39 !default;
$redLastfm:        		#c3000d !default;
$redPinterest:        	#c8232c !default;
$redYouTube:        	#c4302b !default;



// * * * Premium * * *
// -------------------------


//Dulux 10

$redRaspberry:   		#bf545a !default;
$redDiva:   			#b5787d !default;
$redRuby:   			#7d3d3e !default;
$redSummer:   			#a05a5c !default;
$redFire:   			#B24848 !default;
$redFountain:   		#A64048 !default;
$redAutumn:   			#A34847 !default;
$redBloom:   			#9D5351 !default;
$redThai:  				#AA5452 !default;
$redVolcanic:		  	#B5473A !default;

//Zurb Foundation 1

$redZurb:        		#c60f13 !default;









<?php }} ?>