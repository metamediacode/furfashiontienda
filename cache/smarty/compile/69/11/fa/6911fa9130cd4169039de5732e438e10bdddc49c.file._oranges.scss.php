<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:34:04
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\admin\themes\default\sass\modules\colors\_oranges.scss" */ ?>
<?php /*%%SmartyHeaderCode:3206353a035bc919e54-16319018%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6911fa9130cd4169039de5732e438e10bdddc49c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\admin\\themes\\default\\sass\\modules\\colors\\_oranges.scss',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3206353a035bc919e54-16319018',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035bc91cac4_60475372',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035bc91cac4_60475372')) {function content_53a035bc91cac4_60475372($_smarty_tpl) {?>// Oranges 14
// -------------------------


//Default

$orange:               	#FF6600 !default;
$orangeDark:            #993D00 !default;
$orangeDarker:          #4C1F00 !default;
$orangeLight:           #FF944D !default;
$orangeLighter:         #FFC299 !default;
$orangeDull:           	#C96B2C !default;
$orangeDuller:         	#A16740 !default;
$orangeBright:          #FF9933 !default;

//Brands

$orangeAmazon:          #e47911 !default;
$orangeBlogger:         #fc4f08 !default;
$orangeGrooveshark:     #f77f00 !default;
$orangeHackerNews:      #ff6600 !default;
$orangeSoundCloud:      #ff7700 !default;
$orangeUbuntu:      	#dd4814 !default;


// * * * Premium * * *
// -------------------------


//Dulux 10

$orangeAddvocate:        #ff6138 !default;
$orangeGinger:     		 #995039 !default;
$orangeAutumn:       	 #BE5F37 !default;
$orangeDawn: 	 		 #E47C6C !default;
$orangeBongo: 	 		 #E66E5B !default;
$orangeMango: 			 #E98762 !default;
$orangeCoral: 			 #EC926F !default;
$orangePaprika: 	 	 #CF5141 !default;
$orangeAfrican: 		 #DB6335 !default;
$orangeFlame: 	 		 #D46946 !default;
$orangeAuburn: 	 		 #DD8251 !default;
$orangeHarvest: 	 	 #EE8C4D !default;
$orangeSands: 			 #EB8830 !default;












<?php }} ?>