<?php /* Smarty version Smarty-3.1.14, created on 2014-06-17 14:33:52
         compiled from "C:\xampp\htdocs\furfashiongroupbarcelona\themes\default-bootstrap\css\scenes.css" */ ?>
<?php /*%%SmartyHeaderCode:603253a035b05be919-19283125%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bd801d57785c6576347054071c3e88ad8e0f95b0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\furfashiongroupbarcelona\\themes\\default-bootstrap\\css\\scenes.css',
      1 => 1397146152,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '603253a035b05be919-19283125',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_53a035b05c6f13_58044026',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53a035b05c6f13_58044026')) {function content_53a035b05c6f13_58044026($_smarty_tpl) {?>/********************************************************
			Scenes Styles
********************************************************/
#scenes .popover-button span {
  display: block;
  font-size: 28px;
  text-align: center; }
  #scenes .popover-button span:before {
    content: "\f0fe";
    font-family: "FontAwesome";
    display: block;
    vertical-align: 5px; }

.thumbs_banner {
  margin: 10px auto;
  padding: 0;
  height: 62px !important;
  width: 100%;
  border-top: 1px solid #d6d4d4;
  border-bottom: 1px solid #d6d4d4; }
  .thumbs_banner .space-keeper {
    width: 21px;
    float: left;
    display: block;
    height: 100%; }
    .thumbs_banner .space-keeper a.prev {
      display: none;
      width: 21px;
      background: #fbfbfb;
      text-decoration: none;
      color: #333333; }
      .thumbs_banner .space-keeper a.prev:before {
        font-family: "FontAwesome";
        content: "\f053";
        vertical-align: middle;
        padding-top: 35px;
        padding-left: 5px; }
    .thumbs_banner .space-keeper a.next {
      float: left;
      display: block;
      width: 21px;
      background: #fbfbfb;
      text-decoration: none;
      color: #333333; }
      .thumbs_banner .space-keeper a.next:before {
        font-family: "FontAwesome";
        content: "\f054";
        vertical-align: middle;
        padding-top: 35px;
        padding-left: 5px; }

#scenes_list {
  overflow: hidden;
  float: left;
  width: 828px; }
  #scenes_list ul {
    list-style-type: none; }
  #scenes_list li {
    float: left; }
  #scenes_list a {
    display: block; }

#scenes a.popover-button {
  display: block;
  position: absolute;
  text-decoration: none; }
#scenes .popover {
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  -ms-border-radius: 0;
  -o-border-radius: 0;
  border-radius: 0;
  border-color: #d6d4d4; }
#scenes .product-image-container,
#scenes .product-name {
  margin-bottom: 15px; }
#scenes div.description {
  margin-bottom: 15px; }
#scenes .button-container {
  margin-bottom: 15px; }
  #scenes .button-container a {
    text-decoration: none; }
#scenes .price {
  margin-bottom: 10px; }
@media (max-width: 1199px) {
  #scenes {
    display: none; } }
<?php }} ?>